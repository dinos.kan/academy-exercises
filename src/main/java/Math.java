public class Math {

    public static void main(String[] args) {

        min(0,-9, 12);
        isEven(0);
        divide(0, 2);
        swap(7,8);
    }

    public static double sum (double a, double b) {
        return a+b;
    }

    public static void divide (double a, double b) {
        if (b == 0) {
            System.out.println("You can't divide by 0.");
        } else System.out.println(a/b);
    }

    public static double average (double a, double b, double c) {
        return  (a+b+c)/3;
    }

    public static void swap (int a, int b) {
        a = a + b;
        b = a - b;
        a = a - b;
        System.out.println("new a is: " + a + ", new b is: " + b);
    }

    public static void isEven (int a){
        if (a % 2 == 0) {
            System.out.println(a+" is even.");
        } else {
            System.out.println(a+" is odd.");
        }
    }

    public static void min (int a, int b, int c){

        int min;
        if (a<b && a<c){
            min = a;
        } else if (b<a && b<c){
            min = b;
        } else min = c;

        System.out.println("min number is: "+min);
    }
}
