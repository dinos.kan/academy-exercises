import java.util.Arrays;

public class Strings {

    public static void main(String[] args) {

        reverseString("agile");
        canReadBackwards("lol");
        normalizeSpaces("Hello        there");
        bin2dec("110001");
        hammingDist("agile", "actor");
        replaceSubstring("kamilopardali", "aek", 3);
        passwordEvaluation("agileAct0r$");
    }

    public static String reverseString (String word){

        char[] wordChars = word.toCharArray();
        String reversed = "";

        for (int i=word.length()-1; i>=0; i--){
            reversed = reversed + wordChars[i];
        }
        System.out.println(reversed);
        return (reversed);
    }

    public static void canReadBackwards (String word){

        String reversedWord = reverseString(word);
        if (word.equals(reversedWord)){
            System.out.println("The word "+word+" can be read backwards");
        } else
            System.out.println("It can't be read backwards");

    }

    public static int hammingDist(String str1, String str2) {
        int i = 0;
        int count = 0;
        if (str1.length() == str2.length()) {
            while (i < str1.length()) {
                if (str1.charAt(i) != str2.charAt(i))
                    count++;
                i++;
            }
            System.out.println(count);
        }else System.out.println("Words have to be of equal length.");
        return count;
    }

    public static void countFreqOfWord (String sentence, String word){

        String words[] = sentence.split(" ");

        double count = 0;
        for (int i = 0; i < words.length; i++)
        {
            if (word.equals(words[i]))
                count++;
        }
        System.out.println(count/words.length);
    }

    public static String normalizeSpaces (String inp){
        String res = inp.replaceAll("\\s+", " ");
        System.out.println(res);
        return res;
    }

    public static void replaceSubstring(String word, String substring, int position){

        if ((substring.length()+position) <= word.length()) {
            String result = word.substring(0,position) + substring + word.substring(position+substring.length());
            System.out.println(result);
        } else {
            System.out.println("Substring at given position is not valid");
        }
    }

    public static void bin2dec(String binary){

        int decimal = Integer.parseInt(binary, 2);
        System.out.println(decimal);
    }

    public static void passwordEvaluation(String password){
        String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
        if (password.matches(pattern)){
            System.out.println("Successful password.");
        } else System.out.println("Password does not matches criteria");
    }
}
