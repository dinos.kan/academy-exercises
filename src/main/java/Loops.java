import java.util.Arrays;

public class Loops {

    public static void main(String[] args) {
//        printOdds();
        int[] numbers = new int[] {1,4,-3,0,56};
        minValue(numbers);
        fibonacci(-9);
        fibonacci(10);
    }

    public static void printOdds (){
        for (int i = 1; i < 100 ; i +=2) {
            System.out.println(i);
        }
    }

    public static void minValue (int[] numbers){
        int k = numbers[0];
        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] < k) {
                k = numbers[i];
            }
        }
        System.out.println("min number in the array is: "+k);
    }

    public static void fibonacci(int fibo){

        if  (fibo > 0){
            int[] fiboNumbers = new int[fibo];
            fiboNumbers[0] = 0;
            fiboNumbers[1] = 1;
            for (int i=2; i<fibo; i++){
                fiboNumbers[i] = fiboNumbers[i-1] + fiboNumbers[i-2];
            }
            System.out.println(Arrays.toString(fiboNumbers));

        } else System.out.println("Give positive number.");
    }
}
